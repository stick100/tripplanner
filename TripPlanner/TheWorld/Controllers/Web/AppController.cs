using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheWorld.Models;
using TheWorld.Services;
using TheWorld.ViewModels;

namespace TheWorld.Controllers.Web
{
    public class AppController : Controller//MUST BE PUBLIC
    {
        private IMailService _mailService;
        private IWorldRepository _repository;

        public AppController(IMailService service, IWorldRepository repostiory)
        {
            _repository = repostiory;
            _mailService = service;
        }

        public IActionResult Index()
        {

            return View();
        }

        [Authorize]
        public IActionResult Trips()
        {
           return View();
        }


        public IActionResult About()
        {
            return View();
        }


        [HttpPost]
        public IActionResult Contact(ContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                
                var email = Startup.Configuration["AppSettings:SiteEmailAddress"];

                if(string.IsNullOrWhiteSpace(email))
                {
                    ModelState.AddModelError("", "Could not send email, configuration problem");

                }
                
                if(_mailService.SendMail(email,
                    email,
                    $"Contact Page from {model.Name}({model.Email})",
                    model.Message)
                )
                {
                    ModelState.Clear();//Clear out data so you don't get dup emails.

                    ViewBag.Message = "Mail Sent. Thanks!";
                }
            }

            return View();
        }

        public IActionResult Contact()
        {
            
            return View();
        }
    }
}
