//simpleControls.js

(function () {
    "use string";

    angular.module("simpleControls", [])
        .directive("waitCursor", waitCursor);

    function waitCursor()
    {
        return {
            scope: {
                show: "=displayWhen"
            },
            restrict: "E",
            templateUrl: "/views/waitCursor.html"
        };
    }

}
)();